'''
                            +-------------+
                            |Ground Control|
                            |    Station   |
                            +-------------+
                                    |
                                    |
                                    v
                            +-------------+
                            | Communication|
                            |    Link     |
                            +-------------+
                                    |
                                    |
                                    v
                            +-------------+
                            |     Rover   |
                            +-------------+
                           /    |    |    \
                         /      |    |      \
                       /        |    |        \
                     /          |    |          \
                   /            |    |            \
                 /              |    |              \
               /                |    |                \
             /                  |    |                  \
           /                    |    |                    \
         /                      |    |                      \
       /                        |    |                        \
     /                          /                    |    |                    \  /
   /    Power       Propulsion    |    |    Sensor & Instruments \
 /   Management   Management      |    |                      /
 +-----------------------------+ |    |             +-------------+
                                 |    |             | Data Storage |
                                 |    |             | & Processing |
                                 |    |             +-------------+
                                 |    |
                                 |    |
                                 v    v
                           +-------------+
                           | Safety Module|
                           +-------------+

Ce diagramme montre les différents composants constituant l'architecture de notre application Mars Rover.
La station de contrôle au sol permet de contrôler le rover et de recevoir des données de ce dernier.
La liaison de communication est responsable de l'envoi et de la réception des données entre la station de contrôle au sol et le mobile.
Le Rover comprend des sous-systèmes tels que la gestion de l'alimentation, la gestion de la propulsion, les capteurs et les instruments et le stockage et le traitement des données.
Le module de sécurité est chargé de surveiller les systèmes du mobile et de prendre des mesures pour protéger le mobile et ses instruments en cas
d'une panne ou d'un autre problème.
'''

'''
La station de contrôle au sol gère les entrées de l'utilisateur et envoie des commandes au Rover.
'''
class GroundControlStation:
    def __init__(self):
        self.rover = Rover()
    def send_command(self, command):
        if self.rover:
            self.rover.handle_command(command)
        else:
            print("No rover connected.")
        

'''
Le composant Rover gère la réception des commandes,contrôle les systèmes du rover et renvoie les données à la station de contrôle au sol
'''
class Rover:
    def __init__(self):
        self.power_management = PowerManagement()
        self.propulsion_management = PropulsionManagement()
        self.sensor_and_instruments = SensorAndInstruments()
        self.data_storage_and_processing = DataStorageAndProcessing()

    def handle_command(self, command):
        if command == "move forward":
            self.power_management.use_power(10)
            self.propulsion_management.move(20)
            self.sensor_and_instruments.take_picture()
            self.data_storage_and_processing.store_data('computer science')

        elif command == "turn left":
            self.power_management.use_power(5)
            self.propulsion_management.turn_left()
            self.sensor_and_instruments.take_readings()
            self.data_storage_and_processing.store_data()

        elif command == "turn right":
            self.power_management.use_power(5)
            self.propulsion_management.turn_right()
            self.sensor_and_instruments.take_readings()
            # self.data_storage_and_processing.store_data()
            # self.safety.check_safety()
    
        else:
            print("Invalid command.")
        


'''
Lien de communication : ce composant gère l'envoi et la réception de données entre
la station de contrôle au sol et le Rover.
'''
class CommunicationLink:
    def __init__(self):
        self.rover = Rover()
        # self.data = DataStorageAndProcessing()
        self.gcs = GroundControlStation()

    def send_command_to_rover(self, command):
        return self.rover.handle_command(command)



'''
Gestion de l'alimentation: ce composant gère la gestion de l'alimentation du Rover.
'''
class PowerManagement:
    def __init__(self):
        self.power_level = 100

    def check_power(self):
        return self.power_level
        
    def use_power(self, amount):
        self.power_level -= amount
        if self.power_level < 0:
            self.power_level = 0
            print("Power level is too low.")
    
    def charge_power(self, amount):
        self.power_level += amount
        if self.power_level > 100:
            self.power_level = 100
            print("Power level is fully charged.")
        


'''
'''
class PropulsionManagement:
    def __init__(self):
        self.speed = 0

    def move(self, speed):
        self.speed = speed
        print(f"Rover is now moving at {self.speed} km/h")

    def stop(self):
        self.speed = 0
        print("Rover has stopped.")



'''
Sensor & Instruments : gère le capteur et les instruments du Rover
'''
class Camera:
    def capture(self):
        return "Image Captured"

class GPS:
    def get_location(self):
        return "Latitude: 40.730610, Longitude: -73.935242"

class Lidar:
    def scan(self):
        return "Scan Complete"

class SensorAndInstruments:
    def __init__(self):
        self.camera = Camera()
        self.gps = GPS()
        self.lidar = Lidar()
    
    def take_picture(self):
        
        return self.camera.capture()

    def get_location(self):
        location = self.gps.get_location()
        return location

    def scan_surroundings(self):
        return self.lidar.scan()


'''
Stockage et traitement des données : ce composant gère le stockage et le traitement des données collectées par le Rover.
'''

class DataStorageAndProcessing:
    def __init__(self):
        self.data = []

    def store_data(self, data):
        self.data.append(data)
        return self.data

    def process_data(self):
        processed_data = {"sensor_name"}
        for dat in self.data:
            if dat["sensor_name"] in processed_data:
                processed_data[dat["sensor_name"]].append(dat["measurement"])
            else:
                processed_data[dat["sensor_name"]] = [dat["measurement"]]
        return processed_data




if __name__ == "__main__":
    # propulsion = PropulsionManagement()
    # print(propulsion.stop())
    # power = PowerManagement()
    # power.use_power(20)
    # print(power.check_power()) # 80

    com_link = CommunicationLink()
    print(com_link.send_command_to_rover("move forward"))

    # data_storage = DataStorageAndProcessing()
    # print(data_storage.store_data(['computer science',"sensor_name"]))
    # print(data_storage.process_data())